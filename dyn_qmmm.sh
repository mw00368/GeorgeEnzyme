#!/bin/bash

#SBATCH --time=00-02:00:00
#SBATCH --account=e89-sur
#SBATCH --partition=standard
#SBATCH --qos=standard
#SBATCH -o %j_%x.o
#SBATCH -e %j_%x.e

#SBATCH -J 1GOG_dyn_qmmm

#SBATCH --distribution=block:block
#SBATCH --hint=nomultithread

#SBATCH --nodes=1
#SBATCH --tasks-per-node=32
#SBATCH --cpus-per-task=4

export WORK=/mnt/lustre/a2fs-work3/work/e89/e89/maxwin
export MWSHPATH=$WORK/MShTools
export GROMAX=$WORK/GromacsOnEUREKA

RUNKEY="dyn_qmmm"

MDP="../mdp/dyn_qmmm.mdp"
SYSKEY=1GOG
TOP_DIR="../top"
GRO_INP="confout.gro"
GRO_DIR="../min"
TOP=$SYSKEY".top"

cd "$WORK/GeorgeEnzyme"

# load cp2k-gromacs
source $GROMAX/load_gmx.sh -a
source $GROMAX/gmx_funcs.sh
if [[ $GMXEXE == "" ]] ; then
	echo "Failed to load gromacs"
	exit 1
fi

# make the folder
mkdir -p $RUNKEY
cd $RUNKEY
rm \#*

# copy in the necessary files
cp -v "$TOP_DIR/"*itp "$TOP_DIR/"*top $GRO_DIR/$GRO_INP ../index.ndx ../vdw.inp .

# run the preprocessor
time gmx_cp2k grompp -f $MDP -c $GRO_INP -r $GRO_INP -p $TOP -o $RUNKEY".tpr" -n index.ndx

# modify the input file to add VDW corrections
cp $RUNKEY.inp start.inp
grep -B1000 "&END XC_FUNCTIONAL" start.inp > before.inp
grep -A1000 "&END XC_FUNCTIONAL" start.inp | grep -v "&END XC_FUNCTIONAL" > after.inp
cat before.inp > final.inp
cat vdw.inp >> final.inp
cat after.inp >> final.inp
sed '179s/DZVP-MOLOPT-GTH/DZVP-MOLOPT-SR-GTH/' final.inp > $RUNKEY.inp

# OMP setup
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PLACES=cores

# actual QM/MM SMD
time srun mdrun_cp2k -npme 0 -ntomp $OMP_NUM_THREADS -s $RUNKEY".tpr"
SRUNOUT=$?

exit $SRUNOUT
