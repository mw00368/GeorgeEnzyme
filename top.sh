#!/bin/bash

source $GROMAX/load_gmx.sh -a
source $GROMAX/gmx_funcs.sh

FIELD="charmm36-custom"
OUTKEY="1GOG"
BOX_VAC=2

IN_PDB="../1GOG_ZN.pdb"

mkdir -p "top"
cd "top"

# do the termini interactively:
# gromax pdb2gmx -l 1 -inter -f $IN_PDB -o $OUTKEY".gro" -ff $FIELD -water spce -p $OUTKEY".top" -i $OUTKEY".itp" #-ignh

use the termini options 0, 0:
gromax pdb2gmx -l 1 -nter "0 0" -f $IN_PDB -o $OUTKEY".gro" -ff $FIELD -water spce -p $OUTKEY".top" -i $OUTKEY".itp" #-ignh

GMX_RET=$?

# User output
if [ $GMX_RET -eq 0 ] ; then
  successOut "Topology generated successfully"
fi

# Print the total charges for each chain
totalCharge $OUTKEY"_Protein_chain_A.itp" "Protein: A"
Q=$?
let 'QTOT = Q'
totalCharge $OUTKEY"_Other_chain_B.itp" "BGAL: B"
Q=$?
let 'QTOT = QTOT + Q'
totalCharge $OUTKEY"_Other_chain_Z.itp" "Zinc: Z"
Q=$?
let 'QTOT = QTOT + Q'
totalCharge $OUTKEY"_Ion_chain_N.itp" "Sodium: N"
Q=$?
let 'QTOT = QTOT + Q'

# User output
if [ $QTOT -ne 0 ] ; then
  warningOut "Toplogy is not charge neutral!"
fi

gromax editconf -l 1 -f $OUTKEY".gro" -o $OUTKEY"_box.gro" -c -d $BOX_VAC -bt cubic
GMX_RET=$?

# User output
if [ $GMX_RET -eq 0 ] ; then
  successOut "Unit cell defined successfully, nanometres of vacuum = "$BOX_VAC
fi

gromax solvate -l 1 -cp $OUTKEY"_box.gro" -cs spc216.gro -o $OUTKEY"_box_sol.gro" -p $OUTKEY".top"
GMX_RET=$?

# User output
if [ $GMX_RET -eq 0 ] ; then
successOut "Solvated successfully"
fi

gromax grompp -l 1 -f ../mdp/ions.mdp -c $OUTKEY"_box_sol.gro" -p $OUTKEY".top" -o ions.tpr
# gromax genion -l 1 -inter -s ions.tpr -o $OUTKEY"_rdy.gro" -p $OUTKEY".top" -pname NA -nname CL -neutral
gromax genion -l 1 "SOL" -s ions.tpr -o $OUTKEY"_rdy.gro" -p $OUTKEY".top" -pname NA -nname CL -neutral

if [ $GMX_RET -eq 0 ] ; then
successOut "System neutralised successfully"
fi

rm \#*
