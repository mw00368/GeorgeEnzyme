# GeorgeEnzyme

Files accompanying Max's January 2023 gromacs QM/MM tutorial.

Access to Archer2 is required. Max's [GromacsOnEureka](https://gitlab.surrey.ac.uk/mw00368/GromacsOnEUREKA) and [MShTools](https://github.com/mwinokan/MShTools) must be set up.

## Enzyme Demo

0. Preparing the input structure

    *Preparing the PDB is not detailed in this tutorial but the general idea is:*

    - Remove water
    - Ensure atoms and residues are named to comply with the chosen Force Field (pdb2gmx will tell you if you do it wrong!)

1. Building the topology

    - run top.sh to build the topology, solvate the system, and neutralise it

2. Creating the index file

    - load the gromacs module:

    source $GROMAX/load_gmx.sh -a

    - Read through ndx.sh, and copy and paste the lines into the terminal to set up the index file

3. Minimisation

    *Minimisation should be performed before any dynamics*

    - Modify min.sh with the correct paths and SLURM credentials
    - queue min.sh

4. Dynamics (MD)

    - Modify dyn_md.sh with the correct paths and SLURM credentials
    - queue dyn_md.sh

5. Dynamics (QM/MM)

    - Modify dyn_qmmm.sh with the correct paths and SLURM credentials
    - queue dyn_qmmm.sh
