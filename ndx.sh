#!/bin/bash

exit 0

source $GROMAX/load_gmx.sh -a

# run this command and then enter lines 12,14,16... when prompted
$GMXEXE make_ndx -f top/1GOG_rdy.gro -o index.ndx

# temperature coupling group 1 (water + anions)
22 | 21

# rename the group
name 26 Water_and_anions

# temperature coupling group 2 (Everythin else)
! 26

# rename the group
name 27 non_Water_and_anions

# Combine CYS228, TYR272, TYR495, HIS496, HIS581, BGAL, ZN2, and remove backbone atoms
ri 228 | ri 272 | ri 495 | ri 496 | ri 581 | r BGAL | r ZN2 # & ! 4

# rename the new group
name 28 QM

# write out the file
q
